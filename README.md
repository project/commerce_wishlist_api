# Commerce Wishlist API

Sandbox to provide a RESTful interface to interact with wishlists in Drupal
Commerce via a lightweight public API.

The module's structure and features are very similar to commerce_cart_api,
despite of working with wishlists instead of carts. See commerce_cart_api
README.md for example code in the meantime.

## Deprecation notice
**This module is no longer needed, as we have the officially supported
[Commerce API module](https://www.drupal.org/project/commerce_api) now, which
has wishlist integration and should be used instead!**
